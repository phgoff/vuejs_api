import Vue from 'vue'
import App from './App.vue'
import { store } from './store/store'
import router from './router'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false
Vue.use(Vuetify)
Vue.use(VueAxios, axios)

const opts = {}

new Vue({
  render: h => h(App),
  store: store,
  router,
  Vuetify: new Vuetify(opts)
}).$mount('#app')
