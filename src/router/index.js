import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/layout/Home'
import About from '@/layout/About'
import Contact from '@/layout/Contact'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/aboutme',
      name: 'aboutme',
      component: About
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    }
  ],
  mode: 'history'
})
