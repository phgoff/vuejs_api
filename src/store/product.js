import axios from 'axios'
const productModule = {
  state: {
    product_name: 'banana',
    productlist: []

  },
  mutations: {
    editProductName (state) {
      state.product_name = 'apple'
    },
    getAllProduct (state) {
      axios.get('http://192.168.75.24:8000/api/products/').then((response) => {
        state.productlist = response.data
      })
    }
  }
}

export default productModule
