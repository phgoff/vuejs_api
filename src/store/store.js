import Vue from 'vue'
import Vuex from 'vuex'
import productModule from './product'

Vue.use(Vuex)
export const store = new Vuex.Store({
  state: {
    count: 1,
    userlist: [{
      firstname: 'Phirathat',
      lastname: 'GOFF'
    },
    {
      firstname: 'name2',
      lastname: 'name2'
    }]
  },
  mutations: {
    addCountfromStore (state) {
      state.count++
    },
    addUser (state, newuser) {
      state.userlist.push(newuser)
      // eslint-disable-next-line no-console
      console.log(state.userlist)
    }
  },
  modules: {
    productModule: productModule
  }
})
